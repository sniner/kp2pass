# kp2pass - Convert KeePass 2.x XML files to [PassWordStore][1]

This Ruby script imports [KeePass][2] 2.x XML files to an existing [pass][1] store.

First, export your KeePass vault to XML. [KeePassXC][3] 2.2 lacks the xml export, you may have to install the original KeePass running on Mono. Make sure you **export to a secure place**, because your credentials will be clear text!

Second, backup you current `pass` password store:

```
$ tar czf ~/password-store.tgz ~/.password-store
```

Now run `kp2pass` to import all entries not already present:

```
$ kp2pass ~/path/to/your/vault.xml
```

To overwrite all entries, use `-f`.

Now you can list all entries in your `pass` password store:

```
$ pass list
```

## Features

* `kp2pass` does not overwrite existing entries, unless asked to do so
* All relevant fields will be preserved, including custom string fields and file attachements.
* Maps group hierarchy to folders.
* Pure YAML files.

The `pass` password store uses individual files for each entry, encrypted with `gpg`. Concerning the file layout, there is only one constraint: the first line has to be the password. For the rest of the file there is no limitation, it is a simple flat text file.

`kp2pass` uses YAML as file format and the exported files are pure and valid YAML. For convenience, the first and second line of the YAML part are username and URL, if present.

An example:

```
myverysecretpassword
---
Username: j.doe
Url: https://example.com/
Notes: |-
  Line one of additional information.
  And line two.
```

The file name consists of KeePass title and username to allow multiple entries for the same site (title). This is subject to change. Spaces and special characters are replaced with underscores.


## TODO

* Expired entries and entries in the recycle bin will be imported too.
* Localization of field names.
* Check for updated entries and ask for overwrite.
* Make inclusion of user name in file name optional.
* Make underscores in file and group names optional.

## Installation

You need to have Ruby >= 2.0 installed.

```
$ git clone https://github.com/sniner/kp2pass.git
$ cd kp2pass
$ gem build sniner-kp2pass.gemspec
$ gem install sniner-kp2pass-0.1.0.gem
```


## Troubleshooting

Make sure `pass` is working:

```
$ pass list
```


## License

Copyright (c) 2017 Stefan Schönberger <mail@sniner.net> aka 'sniner'.

Released under GPL v3 license. See [license file](./LICENSE.txt) for details.


[1]: https://www.passwordstore.org/
[2]: http://keepass.info/
[3]: https://keepassxc.org/
[4]: https://git.zx2c4.com/password-store/
