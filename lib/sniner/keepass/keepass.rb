require 'nokogiri'
require 'yaml'

module Sniner

    class KeePass
        def self.load_file(path)
            kp = KeePass.new
            kp.load_file(path)
        end

        def self.load_xml(xml)
            kp = KeePass.new
            kp.load_xml(xml)
        end

        def initialize
            @items = {}
        end

        def count
            @items.length
        end

        def items
            @items.values
        end

        def each
            return Enumerator.new(self) unless block_given?
            @items.values.sort_by {|item| item.full_path}.each {|item| yield item}
        end

        # XML layout:
        # <KeePassFile>
        #   <Root>
        #     <Group>
        #       <Entry></Entry>
        #       <Group>
        #         <Entry></Entry>
        #       </Group
        #     </Group>
        #   </Root>
        # </KeePassFile>
        def load_xml(xml)
            doc = Nokogiri.XML(xml)
            doc.xpath("//Group/Entry").each do |entry|
                meta = KeePassXml.meta_data(entry)
                fields = KeePassXml.user_data(entry)
                groups = KeePassXml.groups(entry)

                uuid = meta[:uuid]
                @items[uuid] = Record.new(groups, fields) if uuid && fields && groups
            end
            self
        end

        def load_file(path)
            return nil unless File.exist?(path)
            File.open(path) {|f| load_xml(f.read)}
            self
        end

        private

        module KeePassXml
            # Collect all 'String' items as hash
            def self.user_data(node, data={})
                node.children.each do |child|
                    case child.name
                    when 'String'
                        f = child.children.map {|n| [n.name.to_sym, n.inner_text]}.to_h
                        if f[:Value] && ! f[:Value].empty? && f[:Key].match(/^[A-Z][a-zA-Z]+/)
                            data[f[:Key].downcase.to_sym] = f[:Value]
                        end
                    end
                end
            rescue => ex
            ensure
                return data
            end

            def self.meta_data(node, data={})
                node.children.each do |child|
                    case child.name
                    when 'String'
                        # This is user data
                    when 'Times'
                        data[:times] = meta_data(child)
                    else
                        n = child.name.downcase.to_sym
                        t = child.inner_text.strip
                        data[n] = t unless t.empty?
                    end
                end
            rescue => ex
            ensure
                return data
            end

            # Returns group hierarchy for a node
            def self.groups(node)
                groups = node.ancestors.reverse.map do |n|
                    case n.name
                    when 'Group'
                        n.xpath('Name').first.inner_text
                    end
                end.compact
                # Purge top group 'Vault'
                groups.first=='Vault' ? groups[1..-1] : groups
            rescue => ex
                []
            end

            def self.uuid(node)
                node.xpath('UUID').first.inner_text
            end
        end

        class Record
            attr_reader :groups, :fields, :meta

            def initialize(groups, fields = {}, meta = {})
                @groups = groups
                @fields = fields
                @meta = meta
            end

            def password
                get(:password)
            end

            def username
                get(:username)
            end

            def title
                get(:title)
            end
            alias :name :title

            def uuid
                meta[:uuid]
            end

            def path
                (groups + [name]).map {|g| clean(g)}.join(File::SEPARATOR)
            end

            def full_path
                if username
                    "#{path}_(#{username})"
                else
                    path
                end
            end

            def [](key)
                @fields[key]
            end
            alias :get :[]

            # Format for passwordstore
            # Username and URL will be always on the top of yaml, if present
            def to_pass
                dict = fields.reject {|k,v| [:password, :title].include? k}
                yml = dict.empty? ? '' : hash_to_yaml(dict, :username, :url)
                "#{get(:password)}\n#{yml}"
            end
            alias :to_s :to_pass

            private

            def ordered_hash(dict, *priorize)
                # This relies on an order-preserving hash (Ruby 2.0 and later)
                # 1. Associate key with order number
                keys = dict.keys.map {|k| [k, priorize.length]}.to_h
                priorize.each_with_index {|k, i| keys[k] = i}
                # 2. Translate keys to human readable text
                keys = keys.map {|k,v| [translate_key(k), v]}.to_h
                dict = dict.map {|k,v| [translate_key(k), v]}.to_h
                # 3. Sort hash according to [order number, key]
                dict.sort_by {|k,v| [keys[k], k]}.to_h
            end

            def hash_to_yaml(dict, *priorize)
                sorted = ordered_hash(dict, *priorize)
                # See: https://stackoverflow.com/questions/1054730/is-it-possible-to-specify-formatting-options-for-to-yaml-in-ruby
                sorted.to_yaml(Indent: 4, UseBlock: true, Separator: '---')
            end

            # Translate symbol to text
            def translate_key(sym)
                # TODO: just a quick shot
                sym.to_s.capitalize
            end

            # Clean up path name items
            def clean(str)
                str.to_s.gsub(/\s+/, '_').gsub(/['"]/, '').gsub(File::SEPARATOR, '_')
            end
        end

    end

end

# vim: set et ts=4 sw=4 ft=ruby:
