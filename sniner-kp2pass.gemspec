Gem::Specification.new do |s|
    s.required_ruby_version = '>= 2.0'
    s.name        = 'sniner-kp2pass'
    s.version     = '0.1.0'
    s.date        = '2017-08-27'
    s.summary     = 'KeePass 2.x XML import to PassWordStore'
    s.description = s.summary
    s.authors     = ['Stefan Schönberger']
    s.email       = ['mail@sniner.net']
    s.homepage    = 'https://github.com/sniner/keepass2pass'
    s.license     = 'GPL-3.0'
    s.files       = Dir.glob('lib/**/*.rb')
    s.executables = Dir.glob('bin/*').map {|f| File.basename(f)}
    s.add_runtime_dependency 'nokogiri', '~> 1.6'
end

# vim: set et sw=4 ts=4 ft=ruby:
